$(document).ready(() => {

  $('#menu').click(() => {
    $('#burger').toggleClass('active');
    $('.menu__item').slideToggle();
    $('.menu--bg-overlay').toggleClass('open');
  });
  $('.menu--bg-overlay').click(() => {
    $('#burger').toggleClass('active');
    $('.menu__item').slideToggle();
    $('.menu--bg-overlay').toggleClass('open');
  });


  let slider;
  let slider1;
  let slider2;
  let slider3;
  let slider4;
  let slider5;
  let slider6;
  let slider7;

  const sliderInitialization = () => {
    $(() => {
      slider = $('#main-slider').bxSlider({
        // mode: 'fade',
        captions: true,
        touchEnabled:false
        // slideWidth: 600
      });

      $('.main-slider-wrapper .bx-wrapper .bx-caption').append('<img src="/images/mobile/slider-control/angle-right-solid.svg"/>');
    });

    slider1 = $('.main-projects-wrap #projects__slider').bxSlider({
      pagerCustom: '#projects__nav-wrap',
      // controls: false,
      touchEnabled:false
    });
    slider2 = $('#profiles__slider').bxSlider({touchEnabled:false});
    slider3 = $('.catalog__slider-top-wrap #projects__slider').bxSlider({
      pagerCustom: '#projects__nav-wrap',
      touchEnabled:false
    });
    slider4 = $('.catalog__slider-top-wrap #projects__slider-catalog').bxSlider({
      controls: true,
      touchEnabled:false
    });
    slider5 = $('.catalog__slider-bottom-wrap #projects__slider').bxSlider({
      controls: true,
      touchEnabled:false
    });
    slider6 = $('.projects #projects__slider').bxSlider({
      pagerCustom: '#projects__nav-wrap',
      touchEnabled:false
    });
    slider7 = $('.projects #projects__slider-projects').bxSlider({
      controls: true,
      touchEnabled:false
    });
  };
  const slidersReload = () => {
    if(slider && slider.length){
      slider.reloadSlider()
    }
    if(slider1 && slider1.length){
      slider1.reloadSlider()
    }
    if(slider2 && slider2.length){
      slider2.reloadSlider()
    }
    if(slider3 && slider3.length){
      slider3.reloadSlider()
    }
    if(slider4 && slider4.length){
      slider4.reloadSlider()
    }
    if(slider5 && slider5.length){
      slider5.reloadSlider()
    }
    if(slider6 && slider6.length){
      slider6.reloadSlider()
    }
    if(slider7 && slider7.length){
      slider7.reloadSlider()
    }
  };
  setTimeout(() => {
    sliderInitialization()
  });

  $( window ).resize(function() {
    slidersReload();
  });

  $('#services-info-btn').on('click', (e) => {
    e.preventDefault();
    $('.services-info__text-wrap').removeClass('services-info__text-wrap--hidden');
    $('#services-info-btn').addClass('services-info__link--hidden');
  })



});

