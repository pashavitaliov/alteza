'use strict';

const gulp = require('gulp');
const watch = require('gulp-watch');
const autoprefixer = require('gulp-autoprefixer');
const uglify = require('gulp-uglify');
const sass = require('gulp-sass');
const cssnano = require('gulp-cssnano');
const sourcemaps = require('gulp-sourcemaps');
const rigger = require('gulp-rigger');
const imagemin = require('gulp-imagemin');
const pngquant = require('imagemin-pngquant');
const del = require('del');
const babel = require('gulp-babel');
const gulpIf = require('gulp-if');
const notify = require('gulp-notify');
const plumber = require('gulp-plumber');
const concat = require('gulp-concat');
const newer = require('gulp-newer');
const browserSync = require('browser-sync').create();
const debug = require('gulp-debug');

const pug = require('gulp-pug');

const isDevelopment = !process.env.NODE_ENV || process.env.NODE_ENV == 'development';

const path = {

    build: {
        html: 'build/',
        js: 'build/js/',
        js_libs: 'build/js/libs',
        css: 'build/css/',
        css_libs: 'build/css/libs',
        img: 'build/img/',
        fonts: 'build/fonts/'
    },
    src: {
        html: 'src/*.html',
        pug: 'src/*.pug',
        js: {
            libs: 'src/js/libs/*.js',
            common: 'src/js/common/*.js'
        },
        style: {
            main: 'src/scss/main.scss',
            libs: 'src/scss/libs/*.css'
        },
        img: ['src/img/**/*.*', '!src/img/**/*.svg'],
        svg: 'src/img/**/*.svg',
        fonts: 'src/fonts/**/*.*'
    },
    watch: {
        html: 'src/**/*.html',
        pug: 'src/**/*.pug',
        js: 'src/js/**/*.js',
        style: 'src/scss/**/*.scss',
        img: 'src/img/**/*.*',
        fonts: 'src/fonts/**/*.*'
    },
    clean: './build'
};

const config = {
    server: {
        baseDir: "./build"
    },
    tunnel: true,
    host: 'localhost',
    port: 9000,
    logPrefix: "Alteza"
};

gulp.task('html:build', function () {
    return gulp.src(path.src.html)
        .pipe(rigger())
        .pipe(gulp.dest(path.build.html))
        .pipe(browserSync.stream({stream: true}));
});
gulp.task('pug:build', function () {
    return gulp.src(path.src.pug)
        .pipe(pug())
        .pipe(gulp.dest(path.build.html))
        .pipe(browserSync.stream({stream: true}));
});


gulp.task('js:build', function () {
    return gulp.src(path.src.js.common)
        .pipe(plumber({
            errorHandler: notify.onError(function (err) {
                return {
                    title: 'Scripts',
                    message: err.message
                };
            })
        }))
        .pipe(gulpIf(isDevelopment, sourcemaps.init()))
        .pipe(babel({
            presets: ['env']
        }))
        .pipe(gulpIf(!isDevelopment, uglify()))
        .pipe(concat('bundle.js'))
        .pipe(gulpIf(isDevelopment, sourcemaps.write()))
        .pipe(gulp.dest(path.build.js))
        .pipe(browserSync.stream({stream: true}));
});

gulp.task('js-libs:build', function () {
    return gulp.src(path.src.js.libs)
        .pipe(newer(path.build.js_libs))
        .pipe(gulpIf(isDevelopment, sourcemaps.init()))
        .pipe(gulpIf(!isDevelopment, uglify()))
        .pipe(gulpIf(isDevelopment, sourcemaps.write()))
        .pipe(gulp.dest(path.build.js_libs))
        // .pipe(debug({title: 'js-libs'}))
        .pipe(browserSync.stream({stream: true}));

});

gulp.task('style:build', function () {
    return gulp.src(path.src.style.main)
        .pipe(plumber({
            errorHandler: notify.onError(function (err) {
                return {
                    title: 'Style',
                    message: err.message
                };
            })
        }))
        .pipe(gulpIf(isDevelopment, sourcemaps.init()))
        .pipe(sass({
            sourceMap: true,
            errLogToConsole: true
        }))
        .pipe(autoprefixer({browsers: ['last 8 versions']}))
        .pipe(cssnano({zindex: false}))
        .pipe(gulpIf(isDevelopment, sourcemaps.write()))
        .pipe(gulp.dest(path.build.css))
        // .pipe(debug({title: 'style'}))
        .pipe(browserSync.stream({stream: true}));
});

gulp.task('style-libs:build', function () {
    return gulp.src(path.src.style.libs)
        .pipe(newer(path.build.css_libs))
        .pipe(cssnano())
        .pipe(gulp.dest(path.build.css_libs))
        .pipe(browserSync.stream({stream: true}));
});

gulp.task('image', function () {
    return gulp.src(path.src.img)
        .pipe(newer(path.build.img))
        .pipe(imagemin({
            progressive: true,
            // svgoPlugins: [{ removeViewBox: false }],
            use: [pngquant()],
            interlaced: true
        }))
        .pipe(gulp.dest(path.build.img))
        // .pipe(debug({title: 'img'}))
        .pipe(browserSync.stream({stream: true}));
});
gulp.task('svg', function () {
    return gulp.src(path.src.svg)
        .pipe(newer(path.build.img))
        .pipe(gulp.dest(path.build.img))
        // .pipe(debug({title: 'svg'}))
        .pipe(browserSync.stream({stream: true}));
});
gulp.task('img:build', gulp.parallel('image', 'svg'));

gulp.task('fonts:build', function () {
    return gulp.src(path.src.fonts)
        .pipe(newer(path.build.fonts))
        .pipe(gulp.dest(path.build.fonts))
        .pipe(browserSync.stream({stream: true}));
});

gulp.task('clean', function () {
    return del(path.clean);
});

gulp.task('watch', function () {
    const watchCallback = function (done) {
        browserSync.reload();
        done();
    };
    // gulp.watch(path.watch.html, gulp.series('html:build'), watchCallback);
    gulp.watch(path.watch.pug, gulp.series('pug:build'), watchCallback);
    gulp.watch(path.watch.style, gulp.series('style:build'), watchCallback);
    // gulp.watch(path.watch.style, gulp.series('style-libs:build'), watchCallback);
    gulp.watch(path.watch.js, gulp.series('js:build'), watchCallback);
    // gulp.watch(path.watch.js, gulp.series('js-libs:build  '), watchCallback);
    gulp.watch(path.watch.img, gulp.series('img:build'), watchCallback);
    gulp.watch(path.watch.fonts, gulp.series('fonts:build'), watchCallback);
});

gulp.task('build', gulp.series('clean', gulp.parallel( 'pug:build', 'js-libs:build', 'js:build', 'style:build', 'style-libs:build', 'fonts:build', 'img:build')));

gulp.task('serve', function () {
    browserSync.init(config);
});

gulp.task('default', gulp.series('build', gulp.parallel('watch', 'serve')));
